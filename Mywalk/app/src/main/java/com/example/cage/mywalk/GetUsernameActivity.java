package com.example.cage.mywalk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class GetUsernameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File file = new File("User_name.txt");
        if(file.exists())
            go_to_mainscreen();
        else
            setContentView(R.layout.activity_get_username);
    }
    public String UserName;
    public void go_to_mainscreen(){
        Intent Set_time = new Intent(this, MainActivity.class);
        startActivity(Set_time);
    }
    public void selectWalkingtime(View view) {
        EditText UserNameInput = (EditText) findViewById(R.id.edit_username);
        UserName = UserNameInput.getText().toString();
        try {
            if (UserName.length() == 0) {
                Toast
                        .makeText(this, "User Name cannot be empty", Toast.LENGTH_LONG)
                        .show();
            }
            else {
                FileOutputStream NameOutput = openFileOutput("User_name.txt", MODE_PRIVATE);
                OutputStreamWriter osw;
                osw = new OutputStreamWriter(NameOutput);
                try {
                    osw.write(UserName);
                    osw.flush();
                    osw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                go_to_mainscreen();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
