//package com.hinf5300.team3.walktalk;
//
//import android.app.Service;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.PixelFormat;
//import android.telephony.PhoneStateListener;
//import android.telephony.TelephonyManager;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.WindowManager;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//import android.os.Handler;
//
//public class MyBroadcastReceiver extends BroadcastReceiver {
//
//    private WindowManager mWindowManager = null;
//    private TextView tv = null;
//    private Context mContext = null;
//    private RelativeLayout mFloatLayout = null;
//    private boolean firstNew = false;
//
//    public MyBroadcastReceiver() {
//    }
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//        // TODO: This method is called when the BroadcastReceiver is receiving
//        //获取context Dec.4
//        mContext = context.getApplicationContext();
//
////        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
////            //监听向外拨打电话
////            Log.i("ln", "正在向外拨打电话");
////            //弹出悬浮界面
//////            popPhone();
////
////        } else {
//            //监听接听电话
//            TelephonyManager phoneManager = (TelephonyManager) mContext.getSystemService(Service.TELEPHONY_SERVICE);
//            phoneManager.listen(listner, PhoneStateListener.LISTEN_CALL_STATE);
////        }
//    }
//
//    /**
//     * 监听手机来电状态的listner
//     */
//    PhoneStateListener listner = new PhoneStateListener() {
//        @Override
//        public void onCallStateChanged(int state, String incomingNumber) {
//            super.onCallStateChanged(state, incomingNumber);
//            switch (state) {
//                case TelephonyManager.CALL_STATE_IDLE://电话挂断状态
//
//                    //DEC.5
//                    Intent i = new Intent();
//                    i.setAction("com.likebamboo.phoneshow.ACTION_END_CALL");
//                    mContext.sendBroadcast(i);
//
//                    Log.i("ln","电话已经被挂断");
//                    popPhoneRemove();//关闭悬浮界面
//
//                    break;
//                case TelephonyManager.CALL_STATE_OFFHOOK://电话接听状态
//                    Log.i("ln", "听筒已经被拿起来");
////                        popPhone();
//                    break;
//                case TelephonyManager.CALL_STATE_RINGING://电话响铃状态
//                    Log.i("ln", "来电话啦,响铃啦");
////                    popPhone();
//                    telRinging(); //DEC.5 打开来电悬浮界面，传送来电号码
//                    break;
//                default:
//                    break;
//            }
//        }
//    };
//
//
//    /**
//     * 打开来电悬浮窗
//     */
//    private void telRinging(){
//        if(!firstNew) {
//            //第一次启动悬浮窗，延迟两秒后显示界面，并且把firstNew设置为true
//            firstNew = true;
//            new Handler().postDelayed(new Runnable(){
//                @Override
//                public void run(){
//                    popPhone();
//                }
//            },2000);
//        }
//        else {
//            //如果悬浮窗已经显示
//        }
//    }
//
//    /**
//     * 悬浮窗的具体实现
//     */
//    private void popPhone() {
//
//        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
//        //获取LayoutParams实例用于设置新view的各项属性
//        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
//        //设置新View为系统顶层窗口，显示在其他一切内容至上
//        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;
//        //设置新View不接受触摸事件
//        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
//        //设置新View不许获得焦点,任何触摸操作都传给下一层
//        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
//
//        //设置新View的高度和宽度
//        params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = getWindowHeight() / 2;
//        //设置新View的停靠位置为顶部水平居中
//        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
//
//        LayoutInflater inflater = LayoutInflater.from(mContext);
//        mFloatLayout = (RelativeLayout)inflater.inflate(R.layout.activity_choose_partner,null);
//        tv = new TextView(mContext);
//        tv.setText("你在拨号，可是你不知道你在打给谁");
//
//        mWindowManager.addView(tv, params);
//    }
//
//
//    /***
//     * 移除悬浮窗
//     */
//
//    private void popPhoneRemove() {
//        if(mFloatLayout!=null){
//            mWindowManager.removeViewImmediate(tv);
//            firstNew = false;
//        }
//        mFloatLayout = null;
//
//        Log.i("ln","View remove已经执行");
//    }
//
//    /**
//     * 获取屏幕高度
//     */
//    private int getWindowHeight() {
//        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
//        DisplayMetrics metric = new DisplayMetrics();
//        wm.getDefaultDisplay().getMetrics(metric);
//        return metric.heightPixels;
//    }
//
//}
