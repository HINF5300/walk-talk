package com.hinf5300.team3.walktalk;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import android.os.Handler;
import java.util.logging.LogRecord;

public class TalkWithPartner extends AppCompatActivity {

    private RelativeLayout countDown;
    private TextView minuteTv, secondTv,partnerIsReadyTv;

    // private MyCountDown myCountDown;
    private String partnerName = "Bob";
//    private Button endButton;
    private AlertDialog rateOrNot = null;
    private AlertDialog.Builder builder = null;
    private Context myContext;
    private Intent mIntent;


    private long mMin = 30;
    private long mSecond = 00;
    private boolean isRun = true;
    private Handler mHandler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            partnerIsReadyTv.setText("Aish is ready to talk and walk with you!");
            if(msg.what == 1){
                computeTime();
                if(mMin<10){
                    minuteTv.setText("0"+mMin+"");
                }else{
                    minuteTv.setText(mMin+"");
                }
                if(mSecond<10){
                    secondTv.setText("0"+mSecond+"");
                }else{
                    secondTv.setText(mSecond+"");
                }
                if(mMin==0&&mSecond==0){
                    countDown = (RelativeLayout)findViewById(R.id.count_down_layout);
                    countDown.setVisibility(View.INVISIBLE);
                    isRun = false;
                    rateAlert();
                }
            }
        }
    };

    /*开启倒计时*/
    private void startRun() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(isRun){
                    try{
                        Thread.sleep(1000);
                        Message message = Message.obtain();
                        message.what = 1;
                        mHandler.sendMessage(message);

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    /*倒计时计算*/
    public void computeTime() {

        mSecond--;
        if(mSecond < 0&&mMin>0){
            mMin--;
            mSecond = 59;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //显示 partner 的姓名
        setTitle("Partner: " + partnerName);
        setContentView(R.layout.activity_talk_with_partner);

        //myContext is used for creating and showing alert
        myContext = TalkWithPartner.this;

        //Get count down minutes and seconds
       minuteTv = (TextView)findViewById(R.id.minute_textView);
       secondTv = (TextView)findViewById(R.id.second_textView);

       partnerIsReadyTv = (TextView)findViewById(R.id.partner_is_ready_textView);

        startRun();
    }


    // 当点击END button 生成并显示alert
    public void rateAlert() {
        builder = new AlertDialog.Builder(myContext);
        rateOrNot = builder.setMessage("Do you want to rate this talk?")
                           .setCancelable(false)
                           .setNegativeButton("No thanks", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   // 窗口消失,程序跳转到Summary
                                   mIntent = new Intent(myContext, SummaryActivity.class);
                                   startActivity(mIntent);
                               }
                           })
                           .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   //跳转到rate界面
                                   mIntent = new Intent(myContext, RatePartnerActivity.class);
                                   startActivity(mIntent);
                               }
                           }).create();
        rateOrNot.show();
    }

}

