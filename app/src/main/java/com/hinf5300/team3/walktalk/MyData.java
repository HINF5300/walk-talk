package com.hinf5300.team3.walktalk;

import android.app.Application;

/**
 * Created by ningluo on 12/6/15.
 */
public class MyData extends Application{
    private static String UserLocalID = null;

    public String getUserLocalID() {
        return UserLocalID;
    }

    public void setUserLocalID(String userLocalID) {
        UserLocalID = userLocalID;
    }
}
