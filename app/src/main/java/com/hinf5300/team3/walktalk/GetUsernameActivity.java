package com.hinf5300.team3.walktalk;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class GetUsernameActivity extends AppCompatActivity {
    public String UserName, PhoneNumber, UserID;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    public int Newuser = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "yhkaAuocuia2PGLsCFWyNotPT8wJx9QQxw93jqV1", "oBf0qV71f7PYNAP9yBMpBHIVHscZcBpyBF20avZr");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Searchlist");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> userlist, ParseException e) {
                if (e == null) {
                    Newuser = userlist.size();
                    if (userlist.size() > 0)
                        go_to_mainscreen();
                    else
                        setContentView(R.layout.activity_get_username);
                }
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

//        if(Ufile.exists())
    }

    public void checklist() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Searchlist");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> userlist, ParseException e) {
                if (e == null) {
                    Newuser = userlist.size();
                }
            }
        });
    }

    public void go_to_mainscreen() {
        Intent Set_time = new Intent(this, MainActivity.class);
        startActivity(Set_time);
    }

    public void selectWalkingtime(View view) {
        EditText UserNameInput = (EditText) findViewById(R.id.edit_username);
        UserName = UserNameInput.getText().toString();
        EditText PhoneInput = (EditText) findViewById(R.id.edit_phonenumber);
        PhoneNumber = PhoneInput.getText().toString();

        if (UserName.length() * PhoneNumber.length() == 0) {
            Toast
                    .makeText(this, "Username & Phone cannot be empty", Toast.LENGTH_LONG)
                    .show();
        } else {

                ParseObject userlist = new ParseObject("Searchlist");
                userlist.put("PhoneNumber", PhoneNumber);
                userlist.put("UserName", UserName);
                userlist.pinInBackground();
 //               userlist.saveEventually();
            //UserID is the objectID for first time use
            //Give UserID to MyData.UserLocalID
                UserID = userlist.getObjectId();
            go_to_mainscreen();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "GetUsername Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.hinf5300.team3.walktalk/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "GetUsername Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.hinf5300.team3.walktalk/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


}