package com.hinf5300.team3.walktalk;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class RatePartnerActivity extends AppCompatActivity {

    private RatingBar mRatingBar = null;
    private TextView  descriptionText = null;
    private Button    rateDoneBtn = null;
    private TextView  reportText = null;
    private Context   mContext = null;
    private Intent mIntent = null;
    private AlertDialog reportAlert = null;
    private AlertDialog.Builder mbuilder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_partner);

        mContext = RatePartnerActivity.this;

        mRatingBar = (RatingBar)findViewById(R.id.rating_bar);
        descriptionText = (TextView)findViewById(R.id.rate_description_text);
        rateDoneBtn = (Button)findViewById(R.id.rate_done_button);
        reportText = (TextView)findViewById(R.id.report_user_text);

        //对RatingBar 的操作做出反应
        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                switch ((int) rating) {
                    case 1:
                        descriptionText.setText("Really Bad");
                        break;
                    case 2:
                        descriptionText.setText("Bad");
                        break;
                    case 3:
                        descriptionText.setText("Normal");
                        break;
                    case 4:
                        descriptionText.setText("Good");
                        break;
                    case 5:
                        descriptionText.setText("Great");
                        break;
                    default:
                        break;
                }
            }
        });


        rateDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext,"Rate succeeded",Toast.LENGTH_SHORT).show();
                mIntent = new Intent(mContext, SummaryActivity.class);
                startActivity(mIntent);
            }
        });

        reportText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateAlert();//调用 generateAlert()
            }
        });
    }

    //生成report alert
    public void generateAlert() {
        mbuilder = new AlertDialog.Builder(mContext);
        reportAlert = mbuilder.setMessage("Are you sure to report user?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(mContext,"Report succeeded",Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        reportAlert.dismiss();//关闭alert
                    }
                }).create();
        reportAlert.show();
    }
}
