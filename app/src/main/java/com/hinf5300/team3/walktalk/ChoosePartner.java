package com.hinf5300.team3.walktalk;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

public class ChoosePartner extends AppCompatActivity implements View.OnClickListener {


    private RelativeLayout countDown;
    private TextView minuteTv, secondTv;
    private AlertDialog rateOrNot = null;
    private AlertDialog.Builder builder = null;
    private Intent mIntent;
    public int timePicked = 0;
    public String partnerName,partnerNumber;

    private long mMin = 0;
    private long mSecond = 00;
    private boolean isRun = true;
    private Handler mHandler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if(msg.what == 1){
                computeTime();
                minuteTv = (TextView)mFloatLayout.findViewById(R.id.minute_textView);
                secondTv = (TextView)mFloatLayout.findViewById(R.id.second_textView);
                if(mMin<10){
                    minuteTv.setText("0"+mMin+"");
                }else{
                    minuteTv.setText(mMin+"");
                }
                if(mSecond<10){
                    secondTv.setText("0"+mSecond+"");
                }else{
                    secondTv.setText(mSecond+"");
                }
                if(mMin==0&&mSecond==0){
                    countDown = (RelativeLayout)mFloatLayout.findViewById(R.id.count_down_layout);
                    countDown.setVisibility(View.INVISIBLE);
                    isRun = false;
                    //rateAlert();
                }
            }
        }
    };


    /**
     * 开启倒计时
     */
    private void startRun() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(isRun){
                    try{
                        Thread.sleep(1000);
                        Message message = Message.obtain();
                        message.what = 1;
                        mHandler.sendMessage(message);

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    /**
     * 倒计时中的运算
     */
    public void computeTime() {
        mSecond--;
        if(mSecond < 0&&mMin>0){
            mMin--;
            mSecond = 59;
        }
    }

    /**
     * 监听手机来电状态的listner
     */
    PhoneStateListener listner = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE://电话挂断状态
                    Log.i("ln","电话已经被挂断");
                    isRun = false;
                    popPhoneRemove();             //关闭悬浮界面

                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK://电话接听状态
                    Log.i("ln","听筒已经被拿起来");
                    telRinging();//当两边已经被连接进入通话界面后打开悬浮界面
                    break;
                case TelephonyManager.CALL_STATE_RINGING://电话响铃状态
                    Log.i("ln", "来电话啦,响铃啦");
//                    popPhone();                       //打开来电悬浮界面
                    telRinging();
                    break;
                default:
                    break;
            }
        }
    };

    private Button btnCallPartner, btnCallBernice, btnCallFang;
    private TextView partnerNameTv;
    private WindowManager mWindowManager = null;
    private Context mContext = null;
    private Boolean firstNew = false;
    private LinearLayout mFloatLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Avalaible Partners:");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_partner);

        Intent intent = this.getIntent();
        timePicked = intent.getIntExtra("timePicked", 0);
        partnerName = intent.getStringExtra("getPartnerName");
        partnerNumber = intent.getStringExtra("getPartnerNumber");
        Log.i("ln", "ChoosePartner收到的timePicked:"+timePicked);
        mMin = timePicked;

        mContext = ChoosePartner.this;
        partnerNameTv = (TextView)findViewById(R.id.partner_name_tv);
        partnerNameTv.setText(partnerName);
        btnCallPartner = (Button)findViewById(R.id.btn_call_partner);
        btnCallPartner.setOnClickListener(this);


        TelephonyManager phoneManager = (TelephonyManager) mContext.getSystemService(Service.TELEPHONY_SERVICE);
        phoneManager.listen(listner, PhoneStateListener.LISTEN_CALL_STATE);
    }

    /**
     * 电话铃响起时打开悬浮窗口
     */
        private void telRinging(){
        if(!firstNew) {
            //第一次启动悬浮窗，延迟两秒后显示界面，并且把firstNew设置为true
            firstNew = true;
            new Handler().postDelayed(new Runnable(){
                @Override
                public void run(){
                    popPhone();
                }
            },1000);
        }
        else {
            //如果悬浮窗已经显示
        }
    }

    /**
     * 实现并打开来电悬浮窗口
     */
    private void popPhone() {

        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        //获取LayoutParams实例用于设置新view的各项属性
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        //设置新View为系统顶层窗口，显示在其他一切内容至上
        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;
        //设置新View不接受触摸事件
        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
        //设置新View不许获得焦点,任何触摸操作都传给下一层
        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        //设置新View的高度和宽度
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = getWindowHeight() / 2;
        //设置新View的停靠位置为顶部水平居中
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;

        //取得LayoutInflater实例
        LayoutInflater inflater = LayoutInflater.from(mContext);
        //通过inflater获得activity_talk_with_partner的layout实例,并将其存在mFloatLayout变量里
        mFloatLayout = (LinearLayout)inflater.inflate(R.layout.activity_talk_with_partner, null);
        //后添加的
        minuteTv = (TextView)mFloatLayout.findViewById(R.id.minute_textView);
        secondTv = (TextView)mFloatLayout.findViewById(R.id.second_textView);
        //动态添加mFloatLayout
        mWindowManager.addView(mFloatLayout,params);

        isRun = true;
        startRun();

    }


    /***
     * 移除来电悬浮窗
     */

    private void popPhoneRemove() {
        if(mFloatLayout != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mWindowManager.removeViewImmediate(mFloatLayout);
                    mFloatLayout = null;
                    firstNew = false;
                    mIntent = new Intent(mContext, RatePartnerActivity.class);
                    startActivity(mIntent);
//                    rateAlert();                    //悬浮窗被移除后弹出是否rate询问
                }
            },2000);
            Log.i("ln", "View remove已经执行");
        }
    }

    /**
     * 获取屏幕高度
     */
    private int getWindowHeight() {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metric = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metric);
        return metric.heightPixels;
    }

    /**
     * onClick 监听和反应--调用makeCall拨打电话
     *
     */
    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_call_partner:
                makeCall(partnerNumber);
                break;
//            case R.id.btn_call_Bernice:
//                makeCall("6172834196");
//                break;
//            case R.id.btn_call_Fang:
//                makeCall("6173355219");
//                break;
            default:
                break;
        }
    }

    /**
     * 调用拨号界面并拨打电话
     * @param numberString
     */
    private void makeCall(String numberString) {
        if (!numberString.equals("")) {
            Uri number = Uri.parse("tel:" + numberString);
            Intent makeCall = new Intent(Intent.ACTION_CALL, number);
            try {
                startActivity(makeCall);
            } catch (android.content.ActivityNotFoundException ex) {

            }
        }
    }

    // 当点击END button 生成并显示alert
    public void rateAlert() {
        builder = new AlertDialog.Builder(mContext);
        rateOrNot = builder.setMessage("Do you want to rate this talk?")
                .setCancelable(false)
                .setNegativeButton("No thanks", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 窗口消失,程序跳转到Summary
                        mIntent = new Intent(mContext, SummaryActivity.class);
                        startActivity(mIntent);
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //跳转到rate界面
                        mIntent = new Intent(mContext, RatePartnerActivity.class);
                        startActivity(mIntent);
                    }
                }).create();
        rateOrNot.show();
    }
}
