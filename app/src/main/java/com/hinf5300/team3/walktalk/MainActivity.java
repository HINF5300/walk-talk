package com.hinf5300.team3.walktalk;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class MainActivity extends AppCompatActivity implements OnClickListener{

    private NumberPicker mNumberPicker = null;
    private Button btnStartWalking = null;
    private TextView clearUserDataTv = null;
    private Intent mIntent;
    public int timePicked = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Set Walking Time");

        setContentView(R.layout.activity_main);

        // timeListStr contains all the options of walking time
        String[] timeListStr = new String[]{"5", "10", "15", "20", "30", "45", "60", "75", "90"};

        mNumberPicker = (NumberPicker)findViewById(R.id.choose_walking_time_picker);
        mNumberPicker.setDisplayedValues(timeListStr);
        mNumberPicker.setMinValue(0);
        mNumberPicker.setMaxValue(timeListStr.length - 1);
        mNumberPicker.setValue(1);
        mNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                switch (newVal) {
                    case 0:
                        timePicked = 5;
                        break;
                    case 1:
                        timePicked = 10;
                        break;
                    case 2:
                        timePicked = 15;
                        break;
                    case 3:
                        timePicked = 20;
                        break;
                    case 4:
                        timePicked = 30;
                        break;
                    case 5:
                        timePicked = 45;
                        break;
                    case 6:
                        timePicked = 60;
                        break;
                    case 7:
                        timePicked = 75;
                        break;
                    case 8:
                        timePicked = 90;
                        break;
                    default:
                        timePicked = 10;
                        break;
                }
            }
        });
        btnStartWalking = (Button)findViewById(R.id.btn_start_walking);
        btnStartWalking.setOnClickListener(this);

        clearUserDataTv = (TextView)findViewById(R.id.clear_user_data_Tv);
        clearUserDataTv.setOnClickListener(this);

    }

    public void go_to_setusername() {
        Intent get_username = new Intent(this, GetUsernameActivity.class);
        startActivity(get_username);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_start_walking:
                Log.i("ln", "numberPikcer取得的值为" + timePicked);
                mIntent = new Intent(MainActivity.this, SearchPartnerActivity.class);
                //把选好的时间绑定在mIntent里等待传出去
                mIntent.putExtra("timePicked", timePicked);
                startActivity(mIntent);
                break;
            case R.id.clear_user_data_Tv:
                ParseObject clearlocal = new ParseObject("Searchlist");
                clearlocal.unpinAllInBackground();
                Log.i("ln", "数据已经删除成功");
                Toast.makeText(MainActivity.this, "User Info Cleared!", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.os.Process.killProcess(android.os.Process.myPid());
//                        System.exit(0);
                    }
                }, 2000);

                break;
            default:
                break;
        }

    }
}


