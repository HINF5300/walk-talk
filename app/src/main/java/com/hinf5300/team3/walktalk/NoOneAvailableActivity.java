package com.hinf5300.team3.walktalk;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

public class NoOneAvailableActivity extends AppCompatActivity {

    Button notifyMeBtn = null;
    Button setWalkTimeBtn = null;
    Context mContext = null;
    Intent mIntent = null;
    MyData myData;
    public String MyinfoID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_one_available);

        mContext = NoOneAvailableActivity.this;

        notifyMeBtn = (Button)findViewById(R.id.notify_me_button);
        setWalkTimeBtn = (Button)findViewById(R.id.set_new_walk_time_button);

        notifyMeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // If user choose to notify, post local information to cloud
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Searchlist");
                //get local data first
                query.fromLocalDatastore();
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> userlist, ParseException e) {
                        //        dlg.dismiss();
                        Log.d("klakla", "get: " + e + userlist.size());
                        if (e == null) {
                            if (userlist.size() > 0) {
                                int i = 0;
                                ParseObject p = userlist.get(i);
                                ParseObject post = new ParseObject("Searchlist");
                                post.put("PhoneNumber", p.getString("PhoneNumber"));
                                post.put("UserName", p.getString("UserName"));
                                post.saveInBackground();
                                MyinfoID = p.getObjectId(); //get my information ID for later delete
                                String cn = userlist.get(i).getClassName();
                                //把MyinfoID存在全局变量中
                                myData = new MyData();
                                myData.setUserLocalID(MyinfoID);
                            }
                        } else {
                            Log.d("score", "Error: " + e.getMessage());
                        }
                    }
                });
                Toast.makeText(mContext, "You will be notified.", Toast.LENGTH_SHORT).show();
                mIntent = new Intent(mContext, TalkWithPartner.class);
                startActivity(mIntent);
            }
        });

        setWalkTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIntent = new Intent(mContext, MainActivity.class);
                startActivity(mIntent);
            }
        });

    }
}
