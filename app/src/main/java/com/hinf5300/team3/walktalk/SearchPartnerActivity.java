package com.hinf5300.team3.walktalk;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class SearchPartnerActivity extends AppCompatActivity {

    private AlertDialog noPartnerFound = null;
    private AlertDialog.Builder mBuilder = null;
    private Context mContext = null;
    private Intent mIntent = null;
    private ImageView iv = null;
    public int timePicked = 0;
    public String UserName, PhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Searching Partner...");
        setContentView(R.layout.activity_search_partner);

        mContext = SearchPartnerActivity.this;
        Intent intent = this.getIntent();
        timePicked = intent.getIntExtra("timePicked",10);
        Log.i("ln","SearchPartner取得传来的值timePicked:"+timePicked);


        //Search a user from cloud first
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Searchlist");
        // query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> userlist, ParseException e) {
                //        dlg.dismiss();
                String id = null ,cn = null;
                Log.d("UserNumber", "get: " + e + userlist.size());
                if (e == null) {
                    if (userlist.size() > 0) {                  //get random partner from cloud
                        int i = new Random().nextInt(userlist.size());
     //                   ParseObject p = userlist.get(i);
                        UserName = userlist.get(i).getString("UserName");
                        PhoneNumber = userlist.get(i).getString("PhoneNumber");
                        id = userlist.get(i).getObjectId();
                        cn = userlist.get(i).getClassName();

                        //Jump to calling screen

                        mIntent = new Intent(mContext, ChoosePartner.class);
                        mIntent.putExtra("timePicked", timePicked);
                        mIntent.putExtra("getPartnerName",UserName);
                        mIntent.putExtra("getPartnerNumber",PhoneNumber);
                        startActivity(mIntent);
                    }else {
                        alert();        //post alert for no one available now
                         }
                }

                }
            });
        }





    // 跳出窗口: Oops..no one is available.
    public void alert() {
        mBuilder = new AlertDialog.Builder(mContext);
        noPartnerFound = mBuilder.setMessage("Oops...no one is available.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //转到 NoOneAvailableActivity 界面
                        mIntent = new Intent(mContext, NoOneAvailableActivity.class);
                        startActivity(mIntent);
                    }
                }).create();
        noPartnerFound.show();
    }

    //调用打电话
    private void makeCall(String numberString) {
        if (!numberString.equals("")) {
            Uri number = Uri.parse("tel:" + numberString);
            Intent makeCall = new Intent(Intent.ACTION_CALL, number);
            try {
                startActivity(makeCall);
            } catch (android.content.ActivityNotFoundException ex) {

            }
        }
    }

}
