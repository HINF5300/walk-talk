package com.hinf5300.team3.walktalk;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SummaryActivity extends AppCompatActivity {

    private Button doneButton;
    private Intent myIntent;
    private Context myContext;
    MyData myData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        myContext = SummaryActivity.this;

        doneButton = (Button)findViewById(R.id.done_button);

        //取回自己的objectID 然后传入 deletequest

        ConnectCloud delmyinfo = new ConnectCloud();
        //delete my information on the cloud
        myData = new MyData();
        String objectID = myData.getUserLocalID();
        if(objectID != null){
        delmyinfo.deletequest(objectID);}
        else{}
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myIntent = new Intent(myContext, MainActivity.class);
                startActivity(myIntent);
            }
        });
    }


}
