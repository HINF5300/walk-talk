package com.example.android.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends ActionBarActivity {

    int quantity = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {

        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        boolean hasWhippedCreamChecked = whippedCreamCheckBox.isChecked();

        CheckBox chocolateChecked = (CheckBox) findViewById(R.id.chocolate_checkbox);
        boolean hasChocolateChecked = chocolateChecked.isChecked();

        EditText customerNameInput = (EditText) findViewById(R.id.customer_name_input);
        String customerName = customerNameInput.getText().toString();

        int price = calculatePrice(hasWhippedCreamChecked, hasChocolateChecked);

        String emailSubject = "Just java order for " + customerName;
        String emailText = creatOrderSummary(customerName, price, hasWhippedCreamChecked, hasChocolateChecked);

        composeEmail(emailSubject, emailText);
    }

    /**
     * This method is called when the + button is clicked.
     */
    public void increment(View view) {

        if(quantity == 20){

            Toast.makeText(getApplicationContext(), "You can't order more than 20 cups.", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity + 1;
        displayQuantity(quantity);
    }

    /**
     * This method is called when the - button is clicked.
     */
    public void decrement(View view) {

        if(quantity == 1) {

            Toast.makeText(getApplicationContext(), "You can't order less than 1 cup.", Toast.LENGTH_SHORT).show();
            return;
        }
        quantity = quantity - 1;
        displayQuantity(quantity);
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity(int number) {
        TextView quantityTextView = (TextView) findViewById(
                R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }


    /**
     * Calculate the price of the order
     *
     * @param whippedCream is whether or not the user wants whipped cream topping
     * @param chocolate is whether or not the user wants chocolate topping
     * @return total price
     */
    private int calculatePrice(boolean whippedCream, boolean chocolate ){
        // Price of 1 cup of coffee
        int basePrice = 5;

        // Add 1$ if the user wants whipped cream
        if(whippedCream){
            basePrice = basePrice + 1;
        }

        // Add 2$ if the user wants chocolate
        if(chocolate){
            basePrice = basePrice + 2;
        }

        // Calculate the total order price by multiplying by quantity
        return basePrice * quantity;
    }

    /**
     * Create summary of the order.
     *
     * @param hasWhippedCreamChecked is whether or not the user wants whipped cream topping
     * @param hasChocolateChecked is whether or not the user wants chocolate topping
     * @param price of the order
     * @return text summary
     */
    private String creatOrderSummary(String customerName, int price, boolean hasWhippedCreamChecked, boolean hasChocolateChecked){
        String summaryMessage = "Name: " + customerName;
        summaryMessage += "\nAdd whipped cream? " + hasWhippedCreamChecked;
        summaryMessage += "\nAdd chocolate ? " + hasChocolateChecked;
        summaryMessage += "\nQuantity: " + quantity;
        summaryMessage += "\nTotal: $" + price;
        summaryMessage += "\nThank you!";
        return summaryMessage;
    }

    public void composeEmail(String subject, String text){
        //Intent intent = new Intent(Intent.ACTION_SENDTO);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SENDTO);
        Uri uri = Uri.parse("mailto: beyondsizz@gmail.com");
        //intent.setData(Uri.parse("mailto: beyondsizz@gmail.com"));
        intent.setData(uri);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text);

        if(intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Toast.makeText(MainActivity.this, "There's no email app", Toast.LENGTH_SHORT).show();
        }
    }

}